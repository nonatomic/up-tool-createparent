# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 07-09-2020
### Added
- Adds a menu option to `Edit > Create Parent for selected` to create an empty GameObject and reparents the selected items within. 

## [1.0.2] - 07-09-2020
#### Changed
- Changed the namespace for the CreateParentForSelected script

## [1.0.3] - 08-09-2020
#### Changed
- Changed the assembly def to only build in editor