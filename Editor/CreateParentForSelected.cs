﻿using UnityEditor;
using UnityEngine;

namespace Nonatomic.Packages.CreateParent.Editor
{
	public class CreateParentForSelected
	{
		[MenuItem("Edit/Create Parent For Selected")]
		private static void Execute()
		{
			var selection = Selection.gameObjects;
			var newParent = new GameObject();
			newParent.name = "New Parent";
		
		
			var newParentTransform = newParent.transform;
		
			foreach (var obj in selection)
			{
				obj.transform.SetParent(newParentTransform);
			}
		}
	
	}
}
